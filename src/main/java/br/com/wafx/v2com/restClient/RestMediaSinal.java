package br.com.wafx.v2com.restClient;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

/**
 * @author Kaio Maximiano
 */

@Path("/mdm-vision")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RegisterRestClient
public interface RestMediaSinal {
		
	@GET
	@Path("/mediaSinal")
	String findMediaSinal(@PathParam("meter") String meterId);
}
