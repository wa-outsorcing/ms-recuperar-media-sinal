package br.com.wafx.v2com.resource;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import br.com.wafx.v2com.restClient.RestMediaSinal;

/**
 * @author Kaio Maximiano
 */
@Path("/v2com")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ResourceMediaSinal {

	@Inject
	@RestClient
	RestMediaSinal restClientMdmVision;
	
	@GET
	@Path("/mediaSinal")
	@Operation(description = "Endpoint REST para buscar media de sinal", summary = "Endpoint REST para buscar media de sinal")
	public Response findMediaSinal(@PathParam("meter") String meterId) {
		String result = restClientMdmVision.findMediaSinal(meterId);
		if (result == null || result.isEmpty())
			return Response.status(Status.NOT_FOUND.ordinal()).build();
		return Response.ok(result).build();
	}
}